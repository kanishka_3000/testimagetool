
#include <QApplication>
#include <testcore/BaseTestSummerizer.h>

#include <TestGraphicsItemCtrl.h>
#include <TestGraphicsItemSimulated.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    testcore::BaseTestSummerizer oTestManager(a);
    TestGraphicsItemCtrl oTGIC(oTestManager);
    TestGraphicsItemSimulated oTGIS(oTestManager);
    oTestManager.StartTests();
    return a.exec();
}
