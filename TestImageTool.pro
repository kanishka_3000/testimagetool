#-------------------------------------------------
#
# Project created by QtCreator 2016-08-02T22:12:54
#
#-------------------------------------------------

QT       += core gui
QT += testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestImageTool
TEMPLATE = app
INCLUDEPATH += ../ImageTool
DEPENDPATH += ../ImageTool

SOURCES += main.cpp \
    TestGraphicsItemCtrl.cpp \
    testcore/BaseTest.cpp \
    testcore/BaseTestSummerizer.cpp \
    TestGraphicsItemSimulated.cpp

HEADERS  += \
    TestGraphicsItemCtrl.h \
    testcore/BaseTest.h \
    testcore/BaseTestSummerizer.h \
    TestGraphicsItemSimulated.h
include(../ImageTool/ImageTool.pri)

RESOURCES += \
    TestImageTool.qrc

