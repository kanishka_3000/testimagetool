#include "TestGraphicsItemSimulated.h"
#include <QListWidgetItem>
#include<QSignalSpy>
TestGraphicsItemSimulated::TestGraphicsItemSimulated(testcore::BaseTestSummerizer &rSummerizer,
                                                     QObject *parent):
    testcore::BaseTest(rSummerizer, parent)
{

}

void TestGraphicsItemSimulated::initTestCase()
{
     o_ItemCtrl.AddImage(QPixmap(":/new/test/Resources/sample1.jpg"), 1, "testing");
     QListWidgetItem* pItem = o_ItemCtrl.p_ItemList->item(0);
     pItem->setCheckState(Qt::Checked);
     QTest::mouseClick(o_ItemCtrl.p_ItemList, Qt::LeftButton);
     o_ItemCtrl.p_ItemList->OnItemClicked(pItem);

     o_ItemCtrl.show();
}

void TestGraphicsItemSimulated::testDrawCircle()
{
    QSignalSpy spy(&o_ItemCtrl, SIGNAL(NotifyCircleAdded(QRectF)));
    drawItem(o_ItemCtrl.btn_Circle, QPoint(100,100),  QPoint(150,150));
    QCOMPARE(spy.count(), 1);

    QList<QVariant> arguments = spy.takeFirst();

    QRectF oRect = arguments.first().toRect();
    qDebug() << oRect;
    QCOMPARE(oRect.x(), 29.0);
    QCOMPARE(oRect.y(), 29.0);
    std::vector<QRectF> vectorRects;
    o_ItemCtrl.GetDrawnCircleItems(vectorRects);
    QCOMPARE(vectorRects.size() ,(size_t)1);
    o_ItemCtrl.ClearAllDrawnItems();
}

void TestGraphicsItemSimulated::testDrawRectMixProgramicAdd()
{
    QSignalSpy spy(&o_ItemCtrl, SIGNAL(NotifyRectAdded(int,QRectF)));
    drawItem(o_ItemCtrl.btn_Rectangle, QPoint(103,104),  QPoint(150,150));
    drawItem(o_ItemCtrl.btn_Rectangle, QPoint(180,190),  QPoint(220,300));
    QCOMPARE(spy.count(), 2);
    QList<QVariant> arguments = spy.at(0);
    int iIndex = arguments.first().toInt();

    QCOMPARE(iIndex, 1);

    arguments = spy.at(1);
    iIndex = arguments.first().toInt();
    QCOMPARE(iIndex, 2);

    QRect oRect(10,10,100,100);
    iIndex = o_ItemCtrl.AddRect(oRect);
    QCOMPARE(iIndex ,3);

    drawItem(o_ItemCtrl.btn_Rectangle, QPoint(220,200),  QPoint(400,380));
    QCOMPARE(spy.count(), 3);
    arguments = spy.at(2);

    iIndex = arguments.first().toInt();
    QCOMPARE(iIndex, 4);

    o_ItemCtrl.ClearAllDrawnItems();

}

void TestGraphicsItemSimulated::testDrawAllItemMixProgramicAdd()
{
    QSignalSpy spyRect(&o_ItemCtrl, SIGNAL(NotifyRectAdded(int,QRectF)));
    QSignalSpy spyElipse(&o_ItemCtrl, SIGNAL(NotifyElipseAdded(int,QRectF)));
    QSignalSpy spyCircle(&o_ItemCtrl, SIGNAL(NotifyCircleAdded(int,QRectF)));
    QSignalSpy spyLine(&o_ItemCtrl, SIGNAL(NotifyLineAdded(int,QLineF)));

    drawItem(o_ItemCtrl.btn_Rectangle, QPoint(103,104),  QPoint(150,150));
    drawItem(o_ItemCtrl.btn_Circle, QPoint(103,104),  QPoint(150,150));
    drawItem(o_ItemCtrl.btn_Elipse, QPoint(103,104),  QPoint(150,150));
    drawItem(o_ItemCtrl.btn_Line, QPoint(103,104),  QPoint(150,150));

    checkItemIndex(spyRect, 1, 0);
    checkItemIndex(spyCircle, 2, 0);
    checkItemIndex(spyElipse, 3, 0);
    checkItemIndex(spyLine, 4, 0);

    QRect oRect(10,10,100,100);
    o_ItemCtrl.AddRect(280, oRect);


    drawItem(o_ItemCtrl.btn_Circle, QPoint(103,104),  QPoint(150,150));
    checkItemIndex(spyCircle, 281, 1);
}

void TestGraphicsItemSimulated::drawItem(QToolButton *pToolButton, QPoint oStartPoint, QPoint oEndPoint)
{
    QTest::mouseClick(o_ItemCtrl.rdo_Draw, Qt::LeftButton);
    QTest::mouseClick(pToolButton, Qt::LeftButton);

    QTest::mousePress(o_ItemCtrl.p_GraphicView->viewport(), Qt::LeftButton, 0, oStartPoint);
    QTest::mouseRelease(o_ItemCtrl.p_GraphicView->viewport(), Qt::LeftButton, 0, oEndPoint);
}

void TestGraphicsItemSimulated::checkItemIndex(QSignalSpy &spy, int iExpectedIndex, int iNumber)
{
    QList<QVariant> arguments = spy.at(iNumber);
    int iIndex = arguments.first().toInt();
    QCOMPARE(iIndex, iExpectedIndex);


}

