#ifndef TESTGRAPHICSITEMSIMULATED_H
#define TESTGRAPHICSITEMSIMULATED_H

#include <QObject>
#include <testcore/BaseTestSummerizer.h>
#include <GraphiItemCtrl.h>
class TestGraphicsItemSimulated : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit TestGraphicsItemSimulated(testcore::BaseTestSummerizer& rSummerizer,
                                       QObject *parent = 0);

    GraphiItemCtrl o_ItemCtrl;
signals:

private slots:
    void initTestCase();
    void testDrawCircle();
    void testDrawRectMixProgramicAdd();
    void testDrawAllItemMixProgramicAdd();
    void cleanupTestCase()
    {  }

protected:
    void drawItem(QToolButton* pToolButton, QPoint oStartPoint, QPoint oEndPoint);
    void checkItemIndex(QSignalSpy& spy, int iExpectedIndex, int iNumber);
};

#endif // TESTGRAPHICSITEMSIMULATED_H
