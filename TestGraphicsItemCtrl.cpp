#include "TestGraphicsItemCtrl.h"

TestGraphicsItemCtrl::TestGraphicsItemCtrl(testcore::BaseTestSummerizer &rSummerizer
                                           , QObject *parent)
    : testcore::BaseTest(rSummerizer, parent)
{

}

void TestGraphicsItemCtrl::initTestCase()
{
    //o_ItemCtrl.show();
    o_ItemCtrl.AddImage(QPixmap(":/new/test/Resources/sample1.jpg"), 1, "testing");
    QRect oRect(10,10,100,100);
    o_ItemCtrl.AddRect(oRect);
    QRect oElipse(50, 10, 50, 100);
    o_ItemCtrl.AddElipse(oElipse);
    o_ItemCtrl.AddElipse(oRect);
    QPointF oPont(45, 50);
    o_ItemCtrl.AddCircle(oPont, 60);
    QLineF oLine(12.45, 15.67, 40.54, 80.33);
    o_ItemCtrl.AddLine(oLine);
}

void TestGraphicsItemCtrl::testGetDrawRectItems()
{
    std::vector<QRectF> vectorRects;
    o_ItemCtrl.GetDrawnRectItems(vectorRects);

    QCOMPARE(vectorRects.size(), (size_t)1);
}

void TestGraphicsItemCtrl::testGetDrawElipseItems()
{
    std::vector<QRectF> vectorRects;
   vectorRects.clear();
   o_ItemCtrl.GetDrawnElipseItems(vectorRects);
   QVERIFY(vectorRects.size() == 2);

   QRectF recI = vectorRects.front();
   QCOMPARE(recI.x(), 10.00);
}

void TestGraphicsItemCtrl::testGetDrwanLineItems()
{
    std::vector<QLineF> vecLines;
    o_ItemCtrl.GetDrawnLineItems(vecLines);

    QCOMPARE(vecLines.size(), (size_t)1);

    QLineF oRetLine = vecLines.front();

    QLineF oLine(12.45, 15.67, 40.54, 80.33);
    QCOMPARE(oRetLine, oLine);
}

void TestGraphicsItemCtrl::testGetDrawnCircleItems()
{

    std::vector<QRectF> vectorRects;
    o_ItemCtrl.GetDrawnCircleItems(vectorRects);
    QCOMPARE(vectorRects.size() ,(size_t)1);
    QRectF recI2 = vectorRects.front();

    QCOMPARE(recI2.x(), double(45 - 60));


    QPointF oPont2(90,90);
    o_ItemCtrl.AddCircle(oPont2, 80);

    vectorRects.clear();
    o_ItemCtrl.GetDrawnCircleItems(vectorRects);
    QCOMPARE(vectorRects.size(), (size_t)2);
}

void TestGraphicsItemCtrl::testGetLeftAndWidth()
{
    float fx0,fy0,fx1,fy1,ftopleft, ftopright,fwidth,fheight;
    fx0 = 60;
    fy0 = 90;
    fx1 = 30;
    fy1 = 35;
    GetLeftAndWidth(fx0, fy0, fx1, fy1,ftopleft,
                    ftopright,fwidth, fheight);

    QCOMPARE(fwidth, (float)125.3);
    QCOMPARE(fheight, (float)125.3);
    QCOMPARE(ftopleft, (float)-2.64982);
    QCOMPARE(ftopright, (float)27.3502);
}

void TestGraphicsItemCtrl::testGetIndex()
{
    o_ItemCtrl.ClearAllDrawnItems();
    int iIndex;
    QRect oRect(10,10,100,100);
    iIndex = o_ItemCtrl.AddRect(oRect);
    QCOMPARE(iIndex ,1);
    QRect oElipse(50, 10, 50, 100);
    iIndex = o_ItemCtrl.AddElipse(oElipse);
    QCOMPARE(iIndex ,2);
    iIndex = o_ItemCtrl.AddElipse(oRect);
    QPointF oPont(45, 50);
    QCOMPARE(iIndex ,3);
    iIndex = o_ItemCtrl.AddCircle(oPont, 60);
     QCOMPARE(iIndex ,4);
    QLineF oLine(12.45, 15.67, 40.54, 80.33);
    iIndex = o_ItemCtrl.AddLine(oLine);
    QCOMPARE(iIndex ,5);

    std::vector<QLineF> vecLines;
    o_ItemCtrl.GetDrawnLineItems(vecLines);

    QCOMPARE(vecLines.size(), (size_t)1);
    QRect oRect2(10,10,100,100);
    o_ItemCtrl.AddRect(5, oRect2);

    vecLines.clear();
    o_ItemCtrl.GetDrawnLineItems(vecLines);

    QCOMPARE(vecLines.size(), (size_t)0);

    std::vector<QRectF> vectorRects;
    vectorRects.clear();
    o_ItemCtrl.GetDrawnElipseItems(vectorRects);
    QCOMPARE(vectorRects.size() , (size_t)2 );

    o_ItemCtrl.AddCircle(100,oPont, 60);
    iIndex = o_ItemCtrl.AddCircle(oPont, 60);

    QCOMPARE(iIndex , 101);
    iIndex = o_ItemCtrl.AddRect(oRect);
    vectorRects.clear();
    o_ItemCtrl.GetDrawnRectItems(vectorRects);

    QCOMPARE(vectorRects.size() , (size_t)3 );
    QCOMPARE(iIndex , 102);

}
