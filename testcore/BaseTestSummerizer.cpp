

#include "BaseTestSummerizer.h"
#include <iostream>
#include <QtTest>
using namespace std;
testcore::BaseTestSummerizer::BaseTestSummerizer(QApplication &parent) :
     i_TotalTestCount(0),i_CurrentTestNumber(0),p_Application(parent)
{

}

void testcore::BaseTestSummerizer::Register(testcore::BaseTest *pTesCase)
{
    connect(pTesCase, SIGNAL(NotifyTestStatus(QString,int)), this, SLOT(OnTestComplete(QString,int)), Qt::UniqueConnection);
    set_AllTests.insert(pTesCase);
    i_TotalTestCount++;
}

void testcore::BaseTestSummerizer::StartTests()
{
    QTimer::singleShot(100, this, SLOT(_Start()));
}

void testcore::BaseTestSummerizer::_Start()
{
    foreach(testcore::BaseTest* pBaseTest, set_AllTests)
    {
        pBaseTest->StartTests();
    }
}

void testcore::BaseTestSummerizer::OnTestComplete(QString sTestName, int iStatus)
{
    i_CurrentTestNumber++;
    cout << i_CurrentTestNumber << " out of " << i_TotalTestCount << " are complate "<< endl;
    if(iStatus == 0)
    {
        lst_PassedTests.push_back(sTestName);
    }
    else
    {
        lst_FailedTests.push_back(sTestName);
    }
    if(i_CurrentTestNumber == i_TotalTestCount)
    {
        //All tests are complete
        OnSummeryPrint();
    }
}

void testcore::BaseTestSummerizer::OnSummeryPrint()
{
    cout << "****************TEST SUMMERRY****************" << endl;
    cout << lst_PassedTests.count() << " out of " << i_TotalTestCount << "Passed" << endl;
    cout << "*********************************************" << endl;
    if(lst_PassedTests.size() > 0)
    {
        cout << "*************Passed Test Cases***************" << endl;
        for( QList<QString>::iterator itr = lst_PassedTests.begin(); itr != lst_PassedTests.end(); itr++)
        {
            cout << qPrintable(*itr) << endl;
        }
    }
    cout << "*********************************************" << endl;
    if(lst_FailedTests.size() > 0)
    {
        cout << "*************Failed Test Cases***************" << endl;
        for( QList<QString>::iterator itr = lst_FailedTests.begin(); itr != lst_FailedTests.end(); itr++)
        {
            cout << qPrintable(*itr) << endl;
        }
    }
    cout << "****************TEST SUMMERRY****************" << endl;
    p_Application.exit(0);
}

