

#include "BaseTest.h"
#include <testcore/BaseTestSummerizer.h>
namespace  testcore {


BaseTest::BaseTest(BaseTestSummerizer &rSummerizer, QObject *parent) : QObject(parent)
{
    rSummerizer.Register(this);
}

void BaseTest::StartTests()
{
    int iStatus=  QTest::qExec(this,0, NULL);
    NotifyTestStatus(metaObject()->className(), iStatus);
}

}
