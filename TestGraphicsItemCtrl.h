#ifndef TESTGRAPHICSITEMCTRL_H
#define TESTGRAPHICSITEMCTRL_H

#include <QObject>
#include <testcore/BaseTest.h>
#include <testcore/BaseTestSummerizer.h>
#include <GraphiItemCtrl.h>
class TestGraphicsItemCtrl : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit TestGraphicsItemCtrl(testcore::BaseTestSummerizer& rSummerizer,
                                  QObject *parent = 0);
    GraphiItemCtrl o_ItemCtrl;
signals:

private slots:
    void initTestCase();

    void testGetDrawnCircleItems();
    void testGetDrawRectItems();
    void testGetDrawElipseItems();
    void testGetDrwanLineItems();
    void testGetLeftAndWidth();
    void testGetIndex();
    void cleanupTestCase()
    {  }
};

#endif // TESTGRAPHICSITEMCTRL_H
